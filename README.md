Spedn
=================================

![logo](/images/spedn-logo-cashwave-144.png "Spedn")

Spedn is a high level smart contracts language for Bitcoin Cash.
It is designed for explicity and safety:

* It is statically typed - detects many errors at compile time
* It is explicitly typed - no guessing what the expression is supposed to return
* It is purely-functional - free of side effects, the common source of bugs
* It has a familiar C-like syntax

[Docs](http://spedn.rtfd.io)